import React, { useContext, useEffect, useState } from "react";
import Register from "./components/Register";
import Login from "./components/Login";
import Table from "./components/Table";
import { UserContext } from "./context/UserContext";

import Header from "./components/Header";

const App = () => {
  const [message, setMessage] = useState("");
  const [token] = useContext(UserContext);
  const getWelcomeMessage = async () => {
    const requestOptions = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(
      process.env.REACT_APP_BACKEND_URL,
      requestOptions
    );
    const data = await response.json();

    if (!response.ok) {
      console.log("something messed up");
    } else {
      console.log(message);
      setMessage(data.message);
    }
  };

  useEffect(() => {
    getWelcomeMessage();
  });
  return (
    <>
      <Header />
      <div className="columns">
        <div className="column"></div>
        <div className="column m-5 is-two-thirds">
          {!token ? (
            <div className="columns">
              <Register />
              <Login />
            </div>
          ) : (
            <Table />
          )}
        </div>
        <div className="column"></div>
      </div>
    </>
  );
};

export default App;
