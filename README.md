# Informations

This project was part of my Master's thesis, what can be found here [here](https://benjaminkomjathy.hu/assets/pdf/Komj%C3%A1thy%20Benjamin%20-%20Diplomamunka.pdf) (written in hungarian)

# Live demo

https://todo.benjaminkomjathy.hu/

# Try-out locally


```
npm install
REACT_APP_BACKEND_URL=http://localhost/api npm start
```