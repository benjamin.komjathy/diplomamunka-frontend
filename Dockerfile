FROM node:16-alpine as build-deps
ADD . /app
WORKDIR /app
ARG REACT_APP_BACKEND_URL


ENV REACT_APP_BACKEND_URL=${REACT_APP_BACKEND_URL}


RUN npm install
RUN npm run build

FROM nginx:1.21.1-alpine
COPY --from=build-deps /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
